FROM golang:1.12-alpine3.9 as builder

RUN apk add --no-cache git pkgconfig bash make g++ librdkafka-dev

WORKDIR /app

ENV GO111MODULE on
COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go build -v -o power-generation-bw producer.go


FROM alpine:3.9

RUN apk add --no-cache librdkafka ca-certificates

COPY --from=builder /app/power-generation-bw /usr/bin/power-generation-bw

ENTRYPOINT ["/usr/bin/power-generation-bw"]
