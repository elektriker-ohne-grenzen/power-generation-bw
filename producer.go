package main

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"gopkg.in/alecthomas/kingpin.v2"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const baseURL = "https://energy-data.appspot.com/energydata/transnet/latest"

func main() {

	conf := kafka.ConfigMap{}

	topic := kingpin.Flag("topic", "Topic").Default("de.transnetbw.energy.generation").String()
	brokers := kingpin.Flag("bootstrap-servers", "Bootstrap broker(s)").Required().String()
	intervalRaw := kingpin.Flag("scrape-interval", "Interval in seconds for scraping data").Default("300").String()

	xconf := kingpin.Flag("property", "CSV separated key=value librdkafka configuration properties").Short('X').String()

	kingpin.Parse()

	interval, err := strconv.Atoi(*intervalRaw)
	if err != nil {
		panic(err)
	}
	conf["bootstrap.servers"] = *brokers

	if len(*xconf) > 0 {
		for _, kv := range strings.Split(*xconf, ",") {
			x := strings.Split(kv, "=")
			if len(x) != 2 {
				panic("-X expects a ,-separated list of confprop=val pairs")
			}
			conf[x[0]] = x[1]
		}
	}
	fmt.Println("Config: ", conf)

	p, err := kafka.NewProducer((*kafka.ConfigMap)(&conf))
	defer p.Close()

	if err != nil {
		fmt.Printf("Failed to create producer: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("Created Producer %v\n", p)

	go func() {
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				m := ev
				if m.TopicPartition.Error != nil {
					fmt.Printf("Delivery failed: %v\n", m.TopicPartition.Error)
				} else {
					fmt.Printf("Delivered message to topic %s [%d] at offset %v\n",
						*m.TopicPartition.Topic, m.TopicPartition.Partition, m.TopicPartition.Offset)
				}

			default:
				fmt.Printf("Ignored event: %s\n", ev)
			}
		}
	}()

	client := http.Client{}

	for {
		response, err := client.Get(baseURL)
		if err != nil {
			log.Fatal(err)
		}
		b, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err)
		}
		p.ProduceChannel() <- &kafka.Message{
			TopicPartition: kafka.TopicPartition{
				Topic:     topic,
				Partition: kafka.PartitionAny,
			},
			Value: b,
		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
}
